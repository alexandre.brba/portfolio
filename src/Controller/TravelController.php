<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TravelController extends AbstractController
{
    /**
     * @Route("/travel", name="travel")
     */
    public function index()
    {
        return $this->render('travel/index.html.twig', [
            'onglet' => 'habits',
        ]);
    }
}
